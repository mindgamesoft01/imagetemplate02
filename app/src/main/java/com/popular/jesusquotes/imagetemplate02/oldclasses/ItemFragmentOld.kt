package com.popular.jesusquotes.imagetemplate02.oldclasses

import android.app.Activity
import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.Menu
import android.webkit.WebView
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.popular.jesusquotes.imagetemplate02.*
import kotlinx.android.synthetic.main.fragment_item.view.*
import java.io.IOException


class ItemFragmentOld : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var itemTitle: MenuItem? = null
    private var wvWebView: WebView? = null
    private var mAdCount:Int = 0
    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    lateinit var vpItemImage: ViewPager
    private lateinit var appInterfaces: AppInterfaces
    private lateinit var mLayoutInflater: LayoutInflater

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mLayoutInflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate the rate_me_layout for this fragment
        val v = inflater.inflate(R.layout.fragment_item, container, false)


/*----------------ACTIONS FOR TOOLBAR-------------------------*/
        //setup pthe action bar
        val act: AppCompatActivity = activity as AppCompatActivity
//        v.my_toolbar.title = "1/"+ItemDataset.item_current.menus.size
        act.setSupportActionBar(v.my_toolbar)

        setHasOptionsMenu(true)
        toolbar = v.my_toolbar
        vpItemImage = v.vpItemImage

/*----------------ACTIONS FOR TOOLBAR-------------------------*/

        val position = ItemDataset.position
        AppUtils().logDebugMsg("IMG Position: ${getCurrentItemPosition().toString()}")
//        Log.e("IMG POSITION:", getCurrentItemPosition().toString())

/*--------------------LOAD THE VIEW PAGER------------------*/
        vpItemImage.adapter = ImagePagerAdapter()
        vpItemImage.currentItem = ItemDataset.position
        toolbar.title = "${getItemPositionFromView(v) + 1}/${getCurrentItemPosition()}"
        vpItemImage.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {



            // optional
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}



            // optional
            override fun onPageSelected(position: Int) {

                mAdCount++
                if (mAdCount % 5 == 0) {
                    AdObject.admob?.loadNextScreen {  }

                }
            }

            // optional
            override fun onPageScrollStateChanged(state: Int) {
                ItemDataset.position = getCurrentItemFromVP()

                toolbar.title = "${getCurrentItemFromVP() + 1}/${getCurrentItemPosition()}"


            }
        })
/*--------------------LOAD THE VIEW PAGER------------------*/
        // loadtexttoBody() //Load the image


        return v

    }


    override fun onResume() {
        super.onResume()
        toolbar.title = "${getCurrentItemFromVP() + 1}/${getCurrentItemPosition()}"
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val mnu = inflater.inflate(R.menu.menu, menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val img_bmp = getBitmapImage()
        when (item.itemId) {

            /*-------------------WALLPAPER----------------------*/
          R.id.btnWallpaper -> {
              val myWallpaperManager = WallpaperManager.getInstance(context)

              try {
                  myWallpaperManager.clear()
                  myWallpaperManager.setBitmap(img_bmp)
                  Toast.makeText(context,
                          "Wallpaper Set Successfully!!", Toast.LENGTH_SHORT)
                          .show()

              } catch (e: IOException) {
                  // TODO Auto-generated catch block
                  Toast.makeText(context,
                          "Setting WallPaper Failed!!", Toast.LENGTH_SHORT)
                          .show()

              }
          }

        /*---SHARE THE ITEM----------------------------------*/
            R.id.btnShare -> {
                AppUtils().shareImage(img_bmp,ctx = context as Context)
            }

        /*---BOOK MARK THE ITEM-----------*/
            R.id.btnBookmark -> {
                ItemDataset.mDbHelper?.addBookMark( imgName = getCurrentItemFromDataset())
//                Toast.makeText(context, "Saved ${ItemDataset.item_current.menus[vpItemImage.currentItem]} Successfully.", Toast.LENGTH_LONG).show()
                Snackbar.make(requireActivity().findViewById(R.id.clMainActivity), "Image ${getCurrentItemFromDataset()} saved successfully.", Snackbar.LENGTH_SHORT).show()


            }

        /*--LOAD THE BOOK MARKS SCREEN-----*/
            R.id.btnBookMarksList ->{
                AdObject.admob?.loadNextScreen {
                    appInterfaces.loadBookMarkMenu()
                }
            }
        /*------------RATE ME --------------*/
            R.id.btnRate ->{
                AppUtils().rateApp(activity as Context)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    /*----------------------New Methods-------------------------*/
    private fun getBitmapImage(): Bitmap {
        var img:Bitmap?=null
        try {
            val uri = getCurrentItemFromDataset()
            if (uri.isNotBlank()) {
                img = BitmapFactory.decodeStream(activity?.assets?.open(getCurrentItemFromDataset()))
            }
        }catch (e:Exception){
                img=getDummyImage()
        }
        return img?:getDummyImage()
    }

    private fun getDummyImage(): Bitmap {
        return BitmapFactory.decodeResource(context?.resources, R.drawable.gallery)
    }

    private fun getItemPositionFromView(v: View): Int {
       return v?.vpItemImage?.currentItem ?:0
    }

    private fun getCurrentItemPosition(): Int {
        return getCurrentItemMenus()?.size ?:0
    }

    private fun getCurrentItemFromDataset(): String {
        return getCurrentItemMenus()[getCurrentItemFromVP()] ?:""
    }

    private fun getCurrentItemFromVP(): Int {
       return vpItemImage?.currentItem ?:0
    }
    private fun getCurrentItemMenus(): ArrayList<String> {
       return ItemDataset.item_current?.menus ?: arrayListOf("","")
    }

    /*----------------------New Methods-------------------------*/

    private inner class ImagePagerAdapter : androidx.viewpager.widget.PagerAdapter() {
        private val mImages = getCurrentItemMenus()

        override fun getCount(): Int {
            return mImages.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
//            return view === `object` as RelativeLayout
            return view === `object` as ImageView
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false)


            val imageView = itemView.findViewById(R.id.suit_image) as ImageView
            val iconPath = ItemDataset.ASSET_URI + mImages[position]
            /*GlideApp.with(activity as FragmentActivity)
//                    .load(Uri.parse(iconPath))
                    .load(R.drawable.topicimg1)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView)*/
//            imageView.load(Uri.parse(iconPath))
//            imageView.setOnTouchListener(MultiTouchListener())
            (container as ViewPager).addView(itemView)

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//            container.removeView(`object` as RelativeLayout)
            container.removeView(`object` as ImageView)
        }
    }




}